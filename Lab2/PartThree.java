import java.util.Scanner;
public class PartThree{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the length of one of the sides of the square");
		int sideSc = sc.nextInt();
		System.out.println("Enter the height of the rectangle");
		int h = sc.nextInt();
		System.out.println("Enter the length of the rectangle");
		int l = sc.nextInt();
		
		int areaSquare = AreaComputations.areaSquare(sideSc);
		System.out.println("The area of the square is " + areaSquare);
		
		AreaComputations ar = new AreaComputations();
		int areaRectangle = ar.areaRectangle(h,l);
		System.out.println("The area of the rectangle is " + areaRectangle);
	}
}