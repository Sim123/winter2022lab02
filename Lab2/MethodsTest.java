public class MethodsTest{
	public static void main(String[] args){
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x + 50);
		methodTwoInputNoReturn(22, 2.2);
		System.out.println("The method no input return int");
		int k = methodNoInputReturnInt();
		System.out.println(k);
		System.out.println("The method sum square root");
		double f = sumSquareRoot(6,3);
		System.out.println(f);
		System.out.println("Printing String length");
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		int h = SecondClass.addOne(50);
		System.out.println(h);
		
		SecondClass sc = new SecondClass();
		int g = sc.addTwo(50);
		System.out.println(g);
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int y){
		System.out.println("Inside the method one input no return");
		System.out.println(y);
	}
	public static void methodTwoInputNoReturn(int z, double w){
		System.out.println("Inside the method two input no return");
		System.out.println(z);
		System.out.println(w);
	}
	public static int methodNoInputReturnInt(){
		return 6;
	}
	public static double sumSquareRoot(int a, int b){
		double f = a + b;
		f = Math.sqrt(f);
		return f;
	}
}